from django.db import models
from django.utils.translation import ugettext_lazy as _


class Event(models.Model):
    owner = models.ForeignKey(
        to='users.User',
        on_delete=models.CASCADE,
        related_name='own_events',
        verbose_name=_('Owner'),
    )
