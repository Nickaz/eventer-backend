from urllib.parse import urljoin

from django.urls import reverse
from django.conf import settings
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.contrib.postgres.fields import JSONField
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from .managers import UserManager

from apps.users.services import avatar_path

__all__ = (
    'User',
)


class User(AbstractBaseUser, PermissionsMixin):
    """Custom user model.

    Attributes:
        username (str):  Username.
        is_active (bool): Can user log in to the system.
        is_staff (bool): Can user access to admin interface.
        date_joined (datetime): Date when the account was created.
        image (file): Profile image.
        events (Manager): Many to many relationship for user events.

    Nested attributes:
        is_superuser (bool): The user can super access to admin UI.
        groups(Manager): The groups this user belongs to.
        user_permissions(Manager): Specified permissions for this user.
        last_login (datetime): Last date when user login to the system.

    """
    EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'email'

    username = models.CharField(
        max_length=255,
        verbose_name=_("Username"),
        default='Unnamed user',
    )
    email = models.EmailField(
        max_length=255,
        unique=True,
        verbose_name=_("Email"),
    )
    is_active = models.BooleanField(
        default=True,
        verbose_name=_("Is active"),
    )
    is_staff = models.BooleanField(
        default=False,
        verbose_name=_("Is staff"),
        help_text=_("The user will have access to admin interface."),
    )
    date_joined = models.DateTimeField(
        auto_now_add=True,
        verbose_name=_("Date joined"),
    )
    image = models.ImageField(
        verbose_name=_('Profile image'),
        upload_to=avatar_path,
        blank=True,
        null=True,
    )
    events = models.ManyToManyField(
        to='events.Event',
        related_name='users',
        verbose_name=_('Events'),
    )

    objects = UserManager()

    class Meta:
        db_table = 'users'
        ordering = ('email',)
        verbose_name = _("User")
        verbose_name_plural = _("Users")

    def __str__(self):
        return f'{self.id} - {self.username} - {self.email}'

    def get_admin_change_url(self) -> str:
        """Get admin change URL.

        Build full url (host + path) to standard Django admin page for
        object like:

            https://api.sitename.com/admin/users/user/234/

        """

        assert self.id, "Instance must have an ID"

        return urljoin(
            settings.DJANGO_SITE_BASE_HOST,
            reverse('admin:users_user_change', args=(self.id,)),
        )
